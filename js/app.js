// var apiKey = '46158722';
// var sessionId = '2_MX40NjE1ODcyMn5-MTUzMjQxMjI3NTYyMn5oWm1HMDBtME9pRXk5bTgvYkFha2R1WWR-fg';
// var token = 'T1==cGFydG5lcl9pZD00NjE1ODcyMiZzaWc9YTIyMDVhOGIwNDkwZmQyZWYzMDI5MWM2ZjhlZWQ4NDAzMjE0ZGEyYjpzZXNzaW9uX2lkPTJfTVg0ME5qRTFPRGN5TW41LU1UVXpNalF4TWpJM05UWXlNbjVvV20xSE1EQnRNRTlwUlhrNWJUZ3ZZa0ZoYTJSMVdXUi1mZyZjcmVhdGVfdGltZT0xNTMyNDEyMjk0Jm5vbmNlPTAuMjA4MTMxODYyNDkxNDczNDImcm9sZT1wdWJsaXNoZXImZXhwaXJlX3RpbWU9MTUzMjQxNTg5NSZpbml0aWFsX2xheW91dF9jbGFzc19saXN0PQ==';

var apiKey, sessionId, token;

var SERVER_BASE_URL = 'https://hl-opentok-server-test.herokuapp.com/';
fetch(SERVER_BASE_URL + 'session').then(function (res) {
    return res.json()
}).then(function (res) {
    apiKey = res.apiKey;
    sessionId = res.sessionId;
    token = res.token;
    initializeSession();
}).catch(handleError);

// initializeSession();

function handleError (error) {
    if (error) {
        alert(error.message);
    }
}

function initializeSession () {
    var session = OT.initSession(apiKey, sessionId);

    // Subscribe to a newly created stream
    session.on('streamCreated', function (event) {
        session.subscribe(event.stream, 'subscriber', {   
            insertMode: 'append',
            width: '100%',
            height: '100%'
        }, handleError);
    });

    // Create a publisher
    var publisher = OT.initPublisher('publisher', {
        insertMode: 'append',
        width: '100%',
        height: '100%'
    }, handleError);

    // Connect to session
    session.connect(token, function (error) {
        if (error) {
            handleError(error);
        } else {
            session.publish(publisher, handleError);
        }
    });
}

